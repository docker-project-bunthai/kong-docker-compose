# Create network
$ docker network create --gateway 172.18.0.1 --subnet 172.18.0.0/16 test

# Start postgres
$ docker-compose up -d postgres

# Konga GUI
# URL: http://0.0.0.0:1337
# Connection Kong admin URL: http://kong:8001

# add service
# `data-service` will point to an api endpoint (http://data-service:8080)
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=data-service' \
    --data 'url=http://data-service:8080'

$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=account-service' \
    --data 'url=http://account-service:8080'

// For OAuth2
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=oauth2' \
    --data 'url=http://account-service:8080'

# add route by hosts
$ curl -i -X POST \
    --url http://localhost:8001/services/data-service/routes \
    --data 'hosts[]=data'

# add route by paths
# strip_path=false
# request from client http://localhost:8000/data
$ curl -i -X POST \
    --url http://localhost:8001/services/data-service/routes \
    --data 'name=dataAPI' \
    --data 'paths[]=/data' \
    --data 'strip_path=false'

  $ curl -i -X GET \
  --url http://localhost:8000/data


# add route by paths
# strip_path=true
# request from client http://localhost:8000/v1/data
# 1st v1 path is kong's path
# 2nd data path is the rest controller's endpoint
$ curl -i -X POST \
  --url http://localhost:8001/services/data-service/routes \
  --data 'name=dataAPI' \
  --data 'paths[]=/v1' \
  --data 'strip_path=true'

  $ curl -i -X GET \
  --url http://localhost:8000/v1/data

# add route by paths
# strip_path=true
# request from client http://localhost:8000/data/data
# 1st data path is kong's path
# 2nd data path is the rest controller's endpoint
$ curl -i -X POST \
    --url http://localhost:8001/services/data-service/routes \
    --data 'name=dataAPI' \
    --data 'paths[]=/data' \
    --data 'strip_path=true'

$ curl -i -X GET \
    --url http://localhost:8000/data/data


# add route by paths
# strip_path=true
# request from client http://localhost:8000/data/data
# 1st data path is kong's path
# 2nd data path is the rest controller's endpoint
$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=accountAPI' \
    --data 'paths[]=/data' \
    --data 'strip_path=false'

$ curl -i -X GET \
    --url http://localhost:8000/data/data


# add route by methods
# default strip_path=true
# request from client http://localhost:8000/data
$ curl -i -X POST \
    --url http://localhost:8001/services/data-service/routes \
    --data 'name=dataAPI' \
    --data 'methods[]=GET'

$ curl -i -X GET \
    --url http://localhost:8000/data

$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=accountAPI' \
    --data 'strip_path=false' \
    --data 'paths[]=/myToken' \
    --data 'paths[]=/myToken/hello' \
    --data 'paths[]=/myToken/hello/free' \
    --data 'methods[]=POST' \
    --data 'methods[]=GET'


$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=free' \
    --data 'strip_path=false' \
    --data 'paths[]=/myToken' \
    --data 'paths[]=/myToken/hello/free' \
    --data 'methods[]=POST' \
    --data 'methods[]=GET'

$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=auth' \
    --data 'strip_path=false' \
    --data 'paths[]=/myToken/hello' \
    --data 'methods[]=POST' \
    --data 'methods[]=GET'

$ curl -i -X POST \
    --url http://localhost:8001/services/oauth2/routes \
    --data 'name=oauth2token' \
    --data 'strip_path=true' \
    --data 'paths[]=/v1' \
    --data 'methods[]=POST'


# add plugin to service
curl -X POST http://localhost:8001/services/data-service/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"

curl -X POST http://localhost:8001/services/account-service/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"

// For OAuth2
curl -X POST http://localhost:8001/services/oauth2/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"

# add plugin to routers
curl -X POST http://localhost:8001/routes/auth/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"

# add consumer
# use the response if: 36a66517-c6a1-4137-b86b-88a377b5f353
curl -X POST http://localhost:8001/consumers/ \
    --data "username=hoho1" \
    --data "custom_id=haha1"

# add application
# d3506ca1-2747-4c80-9efe-b1c2998a6631
curl -X POST http://localhost:8001/consumers/d3506ca1-2747-4c80-9efe-b1c2998a6631/oauth2 \
    --data "name=TestApplication" \
    --data "client_id=haha1" \
    --data "client_secret=haha1-secret" \
    --data "redirect_uris=https://www.pi-pe.co.jp/"

# Access token
curl -X GET http://localhost:8000/oauth2/token \
    --header "X-Forwarded-Proto=https" \
    --data "provision_key=Xzna53PPFvBTAb47DzSTYDBbkAaNrac0" \
    --data "client_id=haha1" \
    --data "client_secret=haha1-secret" \
    --data "grant_type=password" \
    --data "authenticated_userid=1" \
    --data "username=hoho1" \
    --data "password=123"

curl -X GET http://localhost:8000/oauth2/token \
    --data "provision_key=hcm7iYgxyazjjh1xSuyH2XDtQhjErfLX" \
    --data "client_id=haha1" \
    --data "client_secret=haha1-secret" \
    --data "grant_type=password" \
    --data "authenticated_userid=1" \
    --data "scope=read"



# Request Token
Note, you must add oauth2 plugin to a service whose strip is true, path = /v1 or else
then you can access to the endpoint `https://localhost:8443/v1/oauth2/token` 
* request from postman
- You must add in docker-compose `kong => ports => 8443:8443`, it will expose an url `https://localhost:8443/v1/oauth2/token`